
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { increment, decrement, reset } from '../redux'
class App extends Component {
  render() {
    return (
      <div >
        <h1>{this.props.count}</h1>
        <div>
          <button onClick={this.props.increment}>+</button>
          <button onClick={this.props.decrement}>-</button>
          <button onClick={this.props.reset}>R</button>
        </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    count: state
  }
}

const mapDispatchToProps = {
  increment: increment,
  decrement: decrement,
  reset: reset
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
